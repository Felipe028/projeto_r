<?php 
include_once "verifica_login.php";
include('chat/config/define.php');
require('chat/classes/BD.class.php');
$sql = BD::getconn()->prepare("SELECT * FROM usuario where idusuario = ?");
$sql->execute(array($_SESSION['idusuario']));
$ln = $sql->fetchObject();
?>
    
<!DOCTYPE html>
<html lang="pt-br">

<head>

  <meta charset="utf-8">
  <meta http-equiv="content-type" content="text/html;charset=utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Amanajé</title>

    
  <link rel="shortcut icon" href="img/logo_1.png">
  <link href="css/style.css" rel="stylesheet" type="text/css">

  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>    
    
  <!-- Custom fonts for this template-->
  <link href="css/a/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="css/a/sb-admin-2.min.css" rel="stylesheet">

</head>    

<body class="body">
        <!-- Topbar -->
        <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">
          <!-- Topbar Navbar -->
                    <!-- Topbar Navbar -->
          <ul class="navbar-nav ml-auto">
		    <li class="nav-item dropdown no-arrow mx-1">
              <a class="nav-link dropdown-toggle" href="index.php?p=perf" id="messagesDropdown" role="button" aria-haspopup="true" aria-expanded="false">
                <i>Projetos</i>
              </a>
			</li>
			
			<li class="nav-item dropdown no-arrow mx-1">
              <a class="nav-link dropdown-toggle" href="chat" id="messagesDropdown" role="button" aria-haspopup="true" aria-expanded="false">
                <i>Amigos</i>
              </a>
			</li>
              
<script type="text/javascript">
$(function(){
   setInterval(function(){
       var id1 = "<?php echo $_SESSION['idusuario']; ?>";
        
        $.post('dropdown/div-chat.php', {id_user: id1}, function(retorno){
            $("#div_chat").html(retorno);
        });	
	}, 3000);
	
	setInterval(function(){       
        $.post('dropdown/div-solicitacoes.php',{}, function(retorno){
            $("#div_solicitacoes").html(retorno);
        });	
	}, 3000);
	
	setInterval(function(){       
        $.post('dropdown/counter_messages_solicitacoes.php',{}, function(retorno){
            $("#count_solicitacoes").html(retorno);
        });	
	}, 3000);
	
	setInterval(function(){       
        $.post('dropdown/counter_messages_chat.php',{}, function(retorno){
            $("#count_chat").html(retorno);
        });	
	}, 3000);
})
</script>
            <li class="nav-item dropdown no-arrow mx-1">
              <a class="nav-link dropdown-toggle" href="chat/index.php" id="messagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i>Chat</i>
                <!-- Counter - Messages -->
				<div id="count_chat"></div>
              </a>
              <!-- Dropdown - Messages -->
              <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="messagesDropdown">
                  <h6 class="dropdown-header">Conversas</h6>
                  <div id="div_chat"></div>
                  <a class="dropdown-item text-center small text-gray-500" href="chat/index.php">Ir para o chat</a>
              </div>
			</li>  

            <!-- Nav Item - Messages -->
            <li class="nav-item dropdown no-arrow mx-1">
              <a class="nav-link dropdown-toggle" href="#" id="messagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i>Solicitações</i>
                <!-- Counter - Messages -->
				<div id="count_solicitacoes"></div>
              </a>
              <!-- Dropdown - Messages -->
              <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="messagesDropdown">
                <h6 class="dropdown-header">
                  Solicitações de Amizade
                </h6>
				<div id="div_solicitacoes"></div>
              </div>
            </li>

            <div class="topbar-divider d-none d-sm-block"></div>

            <!-- Nav Item - User Information -->
            <li class="nav-item dropdown no-arrow">
              <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="mr-2 d-none d-lg-inline text-gray-600 small"><?php echo $ln->nome;?></span>
                
				<?php if($ln->foto != NULL || $ln->foto != ''){ ?>
						<img src="foto_perfil/<?php echo $ln->foto;?>" class="img-profile rounded-circle"/>
						<?php
					  }else{ ?>
						<img src="foto_perfil/default.jpg" class="img-profile rounded-circle"/>
						<?php
					  } ?>
              </a>
              <!-- Dropdown - User Information -->
              <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                <a class="dropdown-item" href="index.php?p=dados">
                  <i></i>
                  Meu Perfil
                </a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="?acao=sair">
                  <i></i>
                  Sair
                </a>
              </div>
            </li>

          </ul>

        </nav>    
<!--<div class="divTopo">
<img src="img/logo_1.png" class="logo"/>
<p class="nomeUniversidade">Universidade Federal do Amazonas</p>
<a href="?acao=sair" class="topoMenu" style="margin: 0px 20px 0px 0px; "><p>Sair</p></a>
<a href="index.php?p=amig" class="topoMenu" style="margin: 0px 20px 0px 0px; "><p>Meus dados</p></a>
<a href="#" class="topoMenu" style="margin: 0px 20px 0px 0px; "><p>Solicitações</p></a>
<a href="chat/index.php" class="topoMenu" style="margin: 0px 20px 0px 0px; "><p>Chat</p></a>
<a href="index.php?p=amig&p2=chat" class="topoMenu" style="margin: 0px 20px 0px 0px; "><p>Amigos</p></a>
<a href="index.php?p=perf" class="topoMenu" style="margin: 0px 20px 0px 0px; "><p>Projetos</p></a>
</div>
-->
<div style="top: 50px; height:30px; margin: 0 auto; width: 90%; texttext-align: center;"> 
<?php
include "pagina.php";
?>
</div>

  <!-- Bootstrap core JavaScript-->
  <script src="js/a/bootstrap.bundle.min.js"></script>
    
</body>
</html>
