<?php
    session_start();

	if(!isset($_SESSION['email'], $_SESSION['idusuario'])){
		header("Location: login.php");
	}
	
	
	
	if(isset($_GET['acao']) && $_GET['acao'] == 'sair'){
		unset($_SESSION['email']);
		unset($_SESSION['idusuario']);
		session_destroy();
		header("Location: login.php");
	}
?>