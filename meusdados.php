<!-- Area Chart -->
            <div class="col-xl-8 col-lg-7" style="margin: 0 auto;">
              <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                  <h6 class="m-0 font-weight-bold text-primary">Meus dados</h6>
                  <div class="dropdown no-arrow">
                    <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <i class="ui-icon ui-icon-caret-1-s"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
                      <a class="dropdown-item" href="#" id="Edi-proj">Editar</a>
                      <a class="dropdown-item" href="#" id="Edi-img">Alterar Imagem</a>
                    </div>
                  </div>
                </div>
                <!-- Card Body -->
                <div class="card-body">
                  <div style="width: 100%; height: 200px;">
				    <div style="width: 200px; height: 100%; float: left;">
					  <?php if($ln->foto != NULL || $ln->foto != ''){ ?>
						<img src="foto_perfil/<?php echo $ln->foto;?>" style="width: 170px; heigth: 170px; margin: 15px 0px 0px 0px;"/>
						<?php
					  }else{ ?>
						<img src="foto_perfil/default.jpg" style="width: 170px; heigth: 170px; margin: 15px 0px 0px 0px;"/>
						<?php
					  } ?>
					</div>
					<div style="height: 100%; width: 50%; min-width: 350px; float: left; text-align: left;">
					  <p style="font-size: 200%; margin: 40px 0px 0px 20px;"><?php echo $ln->nome;?></p>
					  <p style="font-size: 120%; margin: 5px 0px 0px 20px;"><?php echo $ln->email;?></p>
					</div>
				  </div>
                </div>
              </div>
            </div>

<!--EDITAR PROJETO-->
<script>
   $( function() {
    var dialog;
 
    dialog = $( "#dialog-formEdiproj" ).dialog({
      autoOpen: false,
      height: 300,
      width: 450,
      modal: true,
      buttons: {
        Alterar: function() {
          $("#meuformdialogEdiproj").submit();
        },
        Cancelar: function() {
          dialog.dialog( "close" );
        }
      },
    });

	$( "#Edi-proj" ).button().on( "click", function() {
      dialog.dialog( "open" );
    });	
  });
</script>

<!--##POPUP##-->
<!---->
<div id="dialog-formEdiproj" title="Editar">
  <form method="post" action="funcoes/usuario/editar.php" id="meuformdialogEdiproj" enctype="multipart/form-data">
      <label for="name">Nome do projeto</label>
      <input type="text" name="nome" id="nome" value="<?php echo $ln->nome; ?>" style="float: right;" class="text ui-widget-content ui-corner-all"><br/>
	  <label for="senhaatual">Senha Atual</label>
      <input type="password" name="senhaatual" id="senhaatual"  style="float: right;" class="text ui-widget-content ui-corner-all"><br/>
	  <label for="novasenha1">Nova Senha</label>
      <input type="password" name="novasenha1" id="novasenha1" style="float: right;" class="text ui-widget-content ui-corner-all"><br/>
	  <label for="novasenha2">Repetir Nova Senha</label>
      <input type="password" name="novasenha2" id="novasenha2" style="float: right;" class="text ui-widget-content ui-corner-all"><br/>
  </form>
</div>
<!--##FIM POPUP##-->	
<!--fim editar de projetos-->

<!--EDITAR PROJETO-->
<script>
   $( function() {
    var dialog;
 
    dialog = $( "#dialog-formEdiimg" ).dialog({
      autoOpen: false,
      height: 160,
      width: 450,
      modal: true,
      buttons: {
        Alterar: function() {
          $("#meuformdialogEdiimg").submit();
        },
        Cancelar: function() {
          dialog.dialog( "close" );
        }
      },
    });

	$( "#Edi-img" ).button().on( "click", function() {
      dialog.dialog( "open" );
    });	
  });
</script>

<!--##POPUP##-->
<!---->
<div id="dialog-formEdiimg" title="Alterar Imagem">
  <form method="post" action="funcoes/usuario/alterar_imagem.php" id="meuformdialogEdiimg" enctype="multipart/form-data">
	  <label for="novasenha2">Foto do Perfil</label>
      <input type="file" name="arquivo" accept="image/jpeg" style="float: right;" class="text ui-widget-content ui-corner-all"><br/>
  </form>
</div>
<!--##FIM POPUP##-->	
<!--fim editar de projetos-->