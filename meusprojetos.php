<?php
if(!isset($_GET['codpr'])){//se cod não existir
    $sql = BD::getconn()->prepare("SELECT * FROM projeto where usuario_idusuario = ? ORDER BY idprojeto DESC LIMIT 1");
	$sql->execute(array($_SESSION['idusuario']));
}else{
	$sql = BD::getconn()->prepare("SELECT * FROM projeto where idprojeto = ? ");
	$sql->execute(array($_GET['codpr']));
}
$ln = $sql->fetchObject();
?>

<script type="text/javascript">
		    $(function(){
				$("#pesquisa").keyup(function(){
					var pesquisa = $(this).val();
					var pesquisa22 = 10;
					if(pesquisa != ''){
						var dados = { palavra : pesquisa, palavra2 : "<?php echo $ln->idprojeto;?>"}

						$.post('busca_amigo.php', dados, function(retorna){
							$(".resultados").html(retorna);
						});
					}
				});
			});						
</script>
<!--ADICIONAR MEMBRO-->
<script>
   $( function() {
    var dialog;
 
    dialog = $( "#dialog-form" ).dialog({
      autoOpen: false,
      height: 500,
      width: 500,
      modal: true,
    });

	$( "#novomembro" ).button().on( "click", function() {
      dialog.dialog( "open" );
    });	
  });
</script>
<!--##POPUP##-->
<!---->
<div id="dialog-form" title="Adicionar membro">
  <form action="" method="post">
      <label for="name">Buscar</label>
      <input type="text" id="pesquisa" placeholder="Digite o nome..." class="text ui-widget-content ui-corner-all">
  </form>

  <ul class="resultados" style="list-style: none; border: none; margin: 0px 0px 0px -40px; width: 98%;"></ul>
</div>
<!--##FIM POPUP##-->	
<!--fim Adicionar Membro-->




<div class="divtopo3">
<p><?php echo $ln->nome; ?></p>
                  <div class="dropdown no-arrow" style="float: right; margin: 5px 20px 0px 0px;">
                    <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <i class="ui-icon ui-icon-caret-1-s"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
                      <a class="dropdown-item" href="#" id="Edi-proj">Editar</a>
                      <a class="dropdown-item" href="#" id="del-proj">Excluir</a>
                    </div>
                  </div>
</div>
<!--EDITAR PROJETO-->
<script>
   $( function() {
    var dialog;
 
    dialog = $( "#dialog-formEdiproj" ).dialog({
      autoOpen: false,
      height: 500,
      width: 400,
      modal: true,
      buttons: {
        Alterar: function() {
          $("#meuformdialogEdiproj").submit();
        },
        Cancelar: function() {
          dialog.dialog( "close" );
        }
      },
    });

	$( "#Edi-proj" ).button().on( "click", function() {
      dialog.dialog( "open" );
    });	
  });
</script>
<!--##POPUP##-->
<!---->
<div id="dialog-formEdiproj" title="Editar">
  <form method="post" action="funcoes/projetos/editar.php" id="meuformdialogEdiproj" enctype="multipart/form-data">
      <label for="name">Nome do projeto</label>
      <input type="text" name="nome" id="nome" value="<?php echo $ln->nome; ?>" class="text ui-widget-content ui-corner-all">
	  <label for="name">Início</label>
      <input type="date" name="inicio" id="inicio" value="<?php echo $ln->inicio; ?>" class="text ui-widget-content ui-corner-all">
	  <label for="name">Término</label>
      <input type="date" name="termino" id="termino"  value="<?php echo $ln->termino; ?>" class="text ui-widget-content ui-corner-all">
	  <label for="name">Descrição</label>
      <textarea name="descricao" id="descricao" class="text ui-widget-content ui-corner-all" rows="6" style="width: 98%;"><?php echo $ln->descricao; ?></textarea>
	  <label for="name">Cronograma</label>
	  <input type="file" name="arquivo" id="arquivo" style="background: #f4f7ee; height: 22px;"/>
	  <input type="text" name="idprojeto" style="display: none;" value="<?php echo $ln->idprojeto;?>"/>
  </form>
</div>
<!--##FIM POPUP##-->	
<!--fim editar de projetos-->
<!--EXCLUSÃO DE PROJETOS-->
<script>
   $( function() {
    var dialog;
 
    dialog = $( "#dialog-formDelproj" ).dialog({
      autoOpen: false,
      height: 160,
      width: 400,
      modal: true,
      buttons: {
        Sim: function() {
          $("#meuformdialogDelproj").submit();
        },
        Não: function() {
          dialog.dialog( "close" );
        }
      },
    });

	$( "#del-proj" ).button().on( "click", function() {
      dialog.dialog( "open" );
    });	
  });
</script>
<!--##POPUP##-->
<!---->
<div id="dialog-formDelproj" title="Excluir">
  <form method="post" action="funcoes/projetos/excluir.php" id="meuformdialogDelproj">
    <p style="margin: 5px;">Deseja excluir o projeto <b><?php echo $ln->nome;?></b>?</p>
	<input type="text" name="cod" style="display: none;" value="<?php echo $ln->idprojeto;?>"/>
  </form>

</div>
<!--##FIM POPUP##-->	
<!--fim exlusao de projetos-->

<div class="divcorpo3_detalhes">
<label for="name">Nome</label>
<p><?php echo $ln->nome; ?></p>
<label for="name">Início</label>
<p><?php echo date('d-m-Y', strtotime($ln->inicio)); ?></p>
<label for="name">Término</label>
<p><?php echo date('d-m-Y', strtotime($ln->termino)); ?></p>
<label for="name">Descrição</label>
<p><?php echo $ln->descricao; ?></p>
<label for="name">Cronograma</label>
<p>
<?php
if($ln->arquivo != NULL){ ?>
	<a href="arquivos/<?php echo $ln->arquivo; ?>" download>Clique aqui para fazer o download do arquivo</a> 
<?php } ?>
</p>
</div>


<div class="divtopo3" style="margin: 10px 0px 0px 0px;">
<p>Membros</p>
                  <div class="dropdown no-arrow" style="float: right; margin: 5px 20px 0px 0px;">
                    <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <i class="ui-icon ui-icon-caret-1-s"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
                      <a class="dropdown-item" href="#" id="novomembro">Adicionar membro</a>
                    </div>
                  </div>
</div>


<div class="divcorpo3_membros">
<?php
$sql_membros = BD::getconn()->prepare("SELECT * FROM usuario_has_projeto where projeto_idprojeto = ?");
$sql_membros->execute(array($ln->idprojeto));
  
//$sqlpro = mysql_query("SELECT * FROM projeto where idprojeto = '".$ln['idprojeto']."'");
//$lnpro = mysql_fetch_array($sqlpro);
?>
<ul style="list-style: none; border: none; margin: 0px 0px 5px -40px;" class="listaprojeto">
<?php
while ($ln_membros = $sql_membros->fetchObject()){
$sql_user = BD::getconn()->prepare("SELECT * FROM usuario where idusuario = ?");
$sql_user->execute(array($ln_membros->usuario_idusuario));
$ln_user = $sql_user->fetchObject();
?>
<li><a href="index.php?p=perf&cod=<?php echo $ln_membros->usuario_idusuario;?>" style="text-decoration:none;"><div class="div" style="padding: 0px 0px 5px 10px; height: 30px;">
<div style="margin: 8px 0px 0px 0px;"><?php echo $ln_user->nome;?></div>
  </div>
  </a>
  <div style="float: right; margin: -35px 0px 0px 0px;">
    <button id="del-user<?php echo $ln_membros->usuario_idusuario;?>" <?php if($_SESSION['idusuario'] == $ln_membros->usuario_idusuario){ ?>disabled="disabled"<?php } ?> class="ui-button ui-widget ui-corner-all ui-button-icon-only" title="Excluir" >
    <span class="ui-icon ui-icon-trash"></span>.</button>
  </div>
</li>
 

   <!--EXCLUSÃO DE PROJETOS-->
<script>
   $( function() {
    var dialog;
 
    dialog = $( "#dialog-formDel<?php echo $ln_membros->usuario_idusuario;?>" ).dialog({
      autoOpen: false,
      height: 200,
      width: 400,
      modal: true,
      buttons: {
        Sim: function() {
          $("#meuformdialogDel<?php echo $ln_membros->usuario_idusuario;?>").submit();
        },
        Não: function() {
          dialog.dialog( "close" );
        }
      },
    });

	$( "#del-user<?php echo $ln_membros->usuario_idusuario;?>" ).button().on( "click", function() {
      dialog.dialog( "open" );
    });	
  });
</script>
<!--##POPUP##-->
<!---->
<div id="dialog-formDel<?php echo $ln_membros->usuario_idusuario;?>" title="Remover">
  <form method="post" action="funcoes/projetos/membros/excluir2.php" id="meuformdialogDel<?php echo $ln_membros->usuario_idusuario;?>">
    <p style="margin: 5px;">Deseja remover o membro <b><?php echo $ln_user->nome;?></b> do projeto?</p>
	<input type="text" name="coduser" style="display: none;" value="<?php echo $ln_membros->usuario_idusuario;?>"/>
	<input type="text" name="codproj" style="display: none;" value="<?php echo $ln->idprojeto;?>"/>
  </form>

</div>
<!--##FIM POPUP##-->	
<!--fim exlusao de projetos-->			

 
<?php } ?>
</ul>
</div>


<link href="chat_grupo/css/style.css" rel="stylesheet" type="text/css">   
<div class="divtopo3" >
<p>Conversas</p>
</div>
<?php
include "chat_grupo/chat-index.php";
?>
