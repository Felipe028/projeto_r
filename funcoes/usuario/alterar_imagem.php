<?php
include_once "../../verifica_login.php";
include_once "../../chat/config/define.php";
require_once('../../chat/classes/BD.class.php');

$sql = BD::getconn()->prepare("SELECT * FROM usuario WHERE idusuario = ?");
$sql->execute(array($_SESSION['idusuario']));
$ln = $sql->fetchObject();

$id = $ln->idusuario;

if($_FILES['arquivo']['name'] != ''){//se algum arquivo (foto, pdf etc) foi selecionado
	if($ln->foto != NULL || $ln->foto != ''){
		unlink("../../foto_perfil/".$ln->foto."");
	}
	
	$arquivo1 = $_FILES['arquivo']['name'];//"arquivo.extensao"
	$file_info = pathinfo($arquivo1);//pega o "arquivo.extensao" e separa
    $arquivo = ($id) .'.'. $file_info['extension'];

	$pastaArquivo = $_FILES['arquivo']['tmp_name'];

	move_uploaded_file($pastaArquivo,"../../foto_perfil/".$arquivo);
	
	$sqlUpD = BD::getconn()->prepare("UPDATE usuario SET foto = ? WHERE idusuario = ? ");
	$sqlUpD->execute(array($arquivo, $_SESSION['idusuario']));
	
	echo "<script> alert( 'Imagem alterada com sucesso!' ); location = '../../index.php?p=dados'; </script>";

}else{
	if($ln->foto != NULL || $ln->foto != ''){
		unlink("../../foto_perfil/".$ln->foto."");
	}
	$sqlUpD = BD::getconn()->prepare("UPDATE usuario SET foto = ? WHERE idusuario = ? ");
	$sqlUpD->execute(array("", $_SESSION['idusuario']));
	echo "<script> alert( 'Foto do perfil removida!' ); location = '../../index.php?p=dados#'; </script>";
}
?>