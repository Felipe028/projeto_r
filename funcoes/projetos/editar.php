<?php
include_once "../../verifica_login.php";
include_once "../../chat/config/define.php";
require_once('../../chat/classes/BD.class.php');

$sql = BD::getconn()->prepare("SELECT * FROM projeto WHERE idprojeto = ?");
$sql->execute(array($_POST['idprojeto']));
$ln = $sql->fetchObject();

$id = $ln->idprojeto;

if($_FILES['arquivo']['name'] != ''){//se algum arquivo (foto, pdf etc) foi selecionado
	if($ln->arquivo != NULL || $ln->arquivo != ''){
		unlink("../../arquivos/".$ln->arquivo."");
	}
	
	$arquivo1 = $_FILES['arquivo']['name'];//"arquivo.extensao"
	$file_info = pathinfo($arquivo1);//pega o "arquivo.extensao" e separa
    $arquivo = ($id) .'.'. $file_info['extension'];

	$pastaArquivo = $_FILES['arquivo']['tmp_name'];

	move_uploaded_file($pastaArquivo,"../../arquivos/".$arquivo);
	
	$sqlUpD = BD::getconn()->prepare("UPDATE projeto SET nome = ?, inicio = ?, termino = ?, descricao = ?, arquivo = ? WHERE idprojeto = ? ");
	$sqlUpD->execute(array($_POST['nome'], $_POST['inicio'], $_POST['termino'], $_POST['descricao'], $arquivo, $_POST['idprojeto']));

}else{
	$sqlUpD = BD::getconn()->prepare("UPDATE projeto SET nome = ?, inicio = ?, termino = ?, descricao = ? WHERE idprojeto = ? ");
	$sqlUpD->execute(array($_POST['nome'], $_POST['inicio'], $_POST['termino'], $_POST['descricao'], $_POST['idprojeto']));
}

echo "<script> alert( 'Projeto alterado com sucesso!' ); location = '../../index.php?p=perf&codpr=$id'; </script>";
?>