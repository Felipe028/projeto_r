<?php
include_once "../../verifica_login.php";
include_once "../../chat/config/define.php";
require_once('../../chat/classes/BD.class.php');

if($_FILES['arquivo']['name'] != ''){//se algum arquivo (foto, pdf etc) foi selecionado
	
	
	$sqlI = BD::getconn()->prepare("INSERT INTO projeto (usuario_idusuario , nome, inicio, termino, descricao) values (?, ?, ?, ?, ?)");
	$sqlI->execute(array($_SESSION['idusuario'], $_POST['nome'], $_POST['inicio'], $_POST['termino'], $_POST['descricao']));
	
	$sql = BD::getconn()->prepare("SELECT * FROM projeto where usuario_idusuario = ? ORDER BY idprojeto DESC LIMIT 1");
	$sql->execute(array($_SESSION['idusuario']));
	$ln = $sql->fetchObject();
	
	$sqlI = BD::getconn()->prepare("INSERT INTO usuario_has_projeto (usuario_idusuario , projeto_idprojeto) values (?, ?)");
	$sqlI->execute(array($_SESSION['idusuario'], $ln->idprojeto));
	
	$id = $ln->idprojeto;
		
		$arquivo1 = $_FILES['arquivo']['name'];//"arquivo.extensao"
		$file_info = pathinfo($arquivo1);//pega o "arquivo.extensao" e separa 
        $arquivo = ($id) .'.'. $file_info['extension'];

		$pastaArquivo = $_FILES['arquivo']['tmp_name'];
		
		move_uploaded_file($pastaArquivo,"../../arquivos/".$arquivo);
		
		$sqlU = BD::getconn()->prepare("UPDATE projeto SET arquivo = ? WHERE idprojeto = ?");	
		$sqlU->execute(array($arquivo, $arquivo));
}else{
	$sqlI = BD::getconn()->prepare("INSERT INTO projeto (usuario_idusuario , nome, inicio, termino, descricao) values (?, ?, ?, ?, ?)");
	$sqlI->execute(array($_SESSION['idusuario'], $_POST['nome'], $_POST['inicio'], $_POST['termino'], $_POST['descricao']));
	
	$sql = BD::getconn()->prepare("SELECT * FROM projeto where usuario_idusuario = ? ORDER BY idprojeto DESC LIMIT 1");
	$sql->execute(array($_SESSION['idusuario']));
	$ln = $sql->fetchObject();
	$sqlI = BD::getconn()->prepare("INSERT INTO usuario_has_projeto (usuario_idusuario , projeto_idprojeto) values (?, ?)");
	$sqlI->execute(array($_SESSION['idusuario'], $ln->idprojeto));
	$id = $ln->idprojeto;
}

echo "<script> alert( 'Projeto cadastrado com sucesso!' ); location = '../../index.php?p=perf&codpr=$id'; </script>";
?>