<?php
include_once "../../../verifica_login.php";
include_once "../../../chat/config/define.php";
require_once('../../../chat/classes/BD.class.php');

$sql = BD::getconn()->prepare("SELECT * FROM projeto WHERE usuario_idusuario = ? AND idprojeto = ?");
$sql->execute(array($_SESSION['idusuario'], $_POST['cod']));

if($sql->rowCount() > 0){
	$sql = BD::getconn()->prepare("INSERT INTO usuario_has_projeto (usuario_idusuario, projeto_idprojeto) values (?, ?)");
	$sql->execute(array($_POST['iduser'], $_POST['cod']));
	$cod = $_POST['cod'];
	echo "<script>   alert('Membro adicionado com sucesso!'); location = '../../../index.php?p=perf&codpr=$cod'; </script>";
}else{
	echo "<script>   alert('Permissão negada!'); location = '../../../index.php'; </script>";	
}

?>