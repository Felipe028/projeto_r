  <style>
    label, input { display:block; }
    input.text { margin-bottom:12px; width:95%; padding: .4em; }
    fieldset { padding:0; border:0; margin-top:25px; }
    h1 { font-size: 1.2em; margin: .6em 0; }
    div#users-contain { width: 100%; margin: 20px 0; }
    div#users-contain table { margin: 1em 0; border-collapse: collapse; width:100%;}
    div#users-contain table td, div#users-contain table th { border: 1px solid #eee; padding: .6em 10px; text-align: left; width:50%;}
    .ui-dialog .ui-state-error { padding: .3em; }
    .validateTips { border: 1px solid transparent; padding: 0.3em; }
  </style>
<!--FIM POPUP-->
<!--Busca-->
<!--<script type="text/javascript" src="js/jquery-1.12.0.min.js"></script>-->
<script type="text/javascript">
		    $(function(){
				$("#pesquisa").keyup(function(){
					var pesquisa = $(this).val();
					var pesquisa22 = 10;
					if(pesquisa != ''){
						var dados = { palavra : pesquisa, palavra2 : "<?php echo $_GET['cod']?>"}
//					    var car = { myCar: "Saturn", getCar: CarTypes("Honda"), special: Sales };

						$.post('busca_amigo.php', dados, function(retorna){
							$(".resultados").html(retorna);
						});
					}
				});
			});						
</script>
<!--Fim Busca-->



<!--ADICIONAR MEMBRO-->
<script>
   $( function() {
    var dialog;
 
    dialog = $( "#dialog-form" ).dialog({
      autoOpen: false,
      height: 500,
      width: 500,
      modal: true,
    });

	$( "#novoprojeto" ).button().on( "click", function() {
      dialog.dialog( "open" );
    });	
  });
</script>
<!--##POPUP##-->
<!---->
<div id="dialog-form" title="Adicionar membro">
  <form action="" method="post">
      <label for="name">Buscar</label>
      <input type="text" id="pesquisa" placeholder="Digite o nome..." class="text ui-widget-content ui-corner-all">
  </form>

  <ul class="resultados" style="list-style: none; border: none; margin: 0px 0px 0px -40px; width: 98%;"></ul>
</div>
<!--##FIM POPUP##-->	
<!--fim Adicionar Membro-->

<button id="novoprojeto" class="ui-button ui-widget ui-corner-all ui-button-icon-only" title="Adicionar membro"><span class="ui-icon ui-icon-plusthick"></span>.</button>



<div style="width: 500px; border: none; margin: 0px auto 0px auto; background: #eeeeee;-moz-border-radius:5px; -webkit-border-radius:5px;">
<?php
$sql = BD::getconn()->prepare("SELECT * FROM usuario_has_projeto where projeto_idprojeto = ?");
$sql->execute(array($_GET['cod']));

$sqlpro = BD::getconn()->prepare("SELECT * FROM projeto where idprojeto = ?");
$sqlpro->execute(array($_GET['cod']));
$lnpro = $sqlpro->fetchObject();
?>
<ul style="list-style: none; border: none; margin: 15px 0px 0px -40px;" class="listaprojeto">
<?php
while ($ln = $sql->fetchObject()){
$sqluser = BD::getconn()->prepare("SELECT * FROM usuario where idusuario = ?");
$sqluser->execute(array($ln->usuario_idusuario));
$lnuser = $sqluser->fetchObject();
?>

<li><a href="index.php?p=perf&cod=<?php echo $ln->usuario_idusuario;?>" style="text-decoration:none;"><div class="div" style="padding: 2px 0px 5px 10px; height: 30px;">
<div style="margin: 8px 0px 0px 0px;"><?php echo $lnuser->nome;?></div>
  </div>
  </a>
  <div style="float: right; margin: -35px 0px 0px 0px;">
    <button id="del-user<?php echo $ln->usuario_idusuario;?>" <?php if($_SESSION['idusuario'] == $ln->usuario_idusuario){ ?>disabled="disabled"<?php } ?> class="ui-button ui-widget ui-corner-all ui-button-icon-only" title="Excluir" >
    <span class="ui-icon ui-icon-trash"></span>.</button>
  </div>
</li>
 

   <!--EXCLUSÃO DE PROJETOS-->
<script>
   $( function() {
    var dialog;
 
    dialog = $( "#dialog-formDel<?php echo $ln->usuario_idusuario;?>" ).dialog({
      autoOpen: false,
      height: 200,
      width: 400,
      modal: true,
      buttons: {
        Sim: function() {
          $("#meuformdialogDel<?php echo $ln->usuario_idusuario;?>").submit();
        },
        Não: function() {
          dialog.dialog( "close" );
        }
      },
    });

	$( "#del-user<?php echo $ln->usuario_idusuario;?>" ).button().on( "click", function() {
      dialog.dialog( "open" );
    });	
  });
</script>
<!--##POPUP##-->
<!---->
<div id="dialog-formDel<?php echo $ln->usuario_idusuario;?>" title="Remover">
  <form method="post" action="funcoes/projetos/membros/excluir2.php" id="meuformdialogDel<?php echo $ln->usuario_idusuario;?>">
    <p style="margin: 5px;">Deseja remover o membro <b><?php echo $lnuser->nome;?></b> do projeto?</p>
	<input type="text" name="coduser" style="display: none;" value="<?php echo $ln->usuario_idusuario;?>"/>
	<input type="text" name="codproj" style="display: none;" value="<?php echo $ln->projeto_idprojeto;?>"/>
  </form>
</div>
<!--##FIM POPUP##-->	
<!--fim exlusao de projetos-->			

 
<?php } ?>
</ul>

</div>