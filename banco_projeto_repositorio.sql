-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: 23-Out-2019 às 12:40
-- Versão do servidor: 5.7.26
-- versão do PHP: 7.2.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `projeto_repositorio`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `amigos`
--

DROP TABLE IF EXISTS `amigos`;
CREATE TABLE IF NOT EXISTS `amigos` (
  `id_amigo` int(10) UNSIGNED NOT NULL,
  `usuario_idusuario` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id_amigo`,`usuario_idusuario`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `amigos`
--

INSERT INTO `amigos` (`id_amigo`, `usuario_idusuario`) VALUES
(1, 2),
(1, 3),
(2, 1),
(3, 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `mensagens`
--

DROP TABLE IF EXISTS `mensagens`;
CREATE TABLE IF NOT EXISTS `mensagens` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_de` int(10) UNSIGNED NOT NULL,
  `id_para` int(10) UNSIGNED NOT NULL,
  `mensagem` text,
  `datahora` datetime DEFAULT NULL,
  `lido` int(10) UNSIGNED DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=65 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `mensagens`
--

INSERT INTO `mensagens` (`id`, `id_de`, `id_para`, `mensagem`, `datahora`, `lido`) VALUES
(64, 1, 2, 'OlÃ¡', '2019-10-22 14:54:17', 1),
(63, 1, 3, 'ola', '2019-10-07 10:56:36', 1),
(62, 1, 3, 'Oi', '2019-10-07 10:56:25', 1),
(61, 1, 3, 'Ei ', '2019-10-07 10:15:21', 1),
(60, 1, 3, 'Oi', '2019-10-07 10:01:18', 1),
(59, 1, 3, 'Bem tbm', '2019-10-07 09:40:40', 1),
(58, 3, 1, 'E vc?', '2019-10-07 09:40:23', 1),
(57, 3, 1, 'Bem ', '2019-10-07 09:40:18', 1),
(56, 1, 3, 'Como vai', '2019-10-07 09:40:12', 1),
(55, 1, 3, 'Oila', '2019-10-07 09:40:08', 1),
(54, 3, 1, 'Oi', '2019-10-07 09:32:43', 1),
(53, 1, 3, 'Oi', '2019-10-07 09:32:34', 1),
(52, 3, 1, 'Ola', '2019-10-07 09:30:57', 1),
(51, 1, 3, 'Oi', '2019-10-07 09:30:42', 1),
(50, 1, 3, 'Ola', '2019-10-07 08:50:55', 1),
(49, 1, 2, 'Oi', '2019-10-07 08:50:40', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `mensagens_grupo`
--

DROP TABLE IF EXISTS `mensagens_grupo`;
CREATE TABLE IF NOT EXISTS `mensagens_grupo` (
  `idmensagens_grupo` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `usuario_idusuario` int(10) UNSIGNED NOT NULL,
  `projeto_idprojeto` int(10) UNSIGNED NOT NULL,
  `mensagem` text,
  `datahora` datetime DEFAULT NULL,
  PRIMARY KEY (`idmensagens_grupo`),
  KEY `mensagens_grupo_FKIndex1` (`projeto_idprojeto`),
  KEY `mensagens_grupo_FKIndex2` (`usuario_idusuario`)
) ENGINE=MyISAM AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `mensagens_grupo`
--

INSERT INTO `mensagens_grupo` (`idmensagens_grupo`, `usuario_idusuario`, `projeto_idprojeto`, `mensagem`, `datahora`) VALUES
(18, 1, 22, 'Tudo bem?', '2019-09-29 11:55:38'),
(17, 1, 22, 'Ola', '2019-09-29 11:55:21'),
(15, 3, 22, 'Mensagem teste 3', '2019-09-27 09:45:31'),
(16, 2, 22, 'Ola', '2019-09-27 09:59:28'),
(14, 1, 22, ',mclkdcds csdjcnsdjcsd cdsncsdc dscdsncsd cdsjcdsnkds csdjcdsjc dscjds csdkjc sdkjsdcjs cdsjc d dsc sdcsducbsd csdcsdcisu csdc', '2019-09-27 08:25:48'),
(13, 3, 22, 'Mensagem 2', '2019-09-26 11:34:24'),
(12, 1, 22, 'Mensagem 1', '2019-09-26 11:31:39'),
(19, 1, 22, 'gyuuyyug', '2019-09-30 11:46:08'),
(20, 1, 22, 'Ola', '2019-10-04 11:06:01'),
(21, 1, 22, 'como vai?', '2019-10-04 11:06:09');

-- --------------------------------------------------------

--
-- Estrutura da tabela `projeto`
--

DROP TABLE IF EXISTS `projeto`;
CREATE TABLE IF NOT EXISTS `projeto` (
  `idprojeto` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `usuario_idusuario` int(10) UNSIGNED NOT NULL,
  `nome` varchar(100) DEFAULT NULL,
  `inicio` date DEFAULT NULL,
  `termino` date DEFAULT NULL,
  `descricao` text,
  `arquivo` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`idprojeto`),
  KEY `projeto_FKIndex1` (`usuario_idusuario`)
) ENGINE=MyISAM AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `projeto`
--

INSERT INTO `projeto` (`idprojeto`, `usuario_idusuario`, `nome`, `inicio`, `termino`, `descricao`, `arquivo`) VALUES
(26, 1, 'A Projeto', '2019-10-17', '2019-10-18', 'Bla bla bla ', NULL),
(25, 1, 'Projeto W', '2019-10-17', '2019-10-31', 'Bla bla bla bla bla', NULL),
(23, 3, 'Projeto Y', '2019-09-23', '2019-09-28', 'Ka ka ka', NULL),
(22, 1, 'Projeto x', '2019-09-23', '2019-09-28', 'Bla bla bla', '22.png');

-- --------------------------------------------------------

--
-- Estrutura da tabela `solicitacoes`
--

DROP TABLE IF EXISTS `solicitacoes`;
CREATE TABLE IF NOT EXISTS `solicitacoes` (
  `idsolicitacoes` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_de` int(10) UNSIGNED DEFAULT NULL,
  `id_para` int(10) UNSIGNED DEFAULT NULL,
  PRIMARY KEY (`idsolicitacoes`)
) ENGINE=MyISAM AUTO_INCREMENT=28 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `solicitacoes`
--

INSERT INTO `solicitacoes` (`idsolicitacoes`, `id_de`, `id_para`) VALUES
(27, 1, 5);

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuario`
--

DROP TABLE IF EXISTS `usuario`;
CREATE TABLE IF NOT EXISTS `usuario` (
  `idusuario` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nome` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `senha` varchar(15) NOT NULL,
  `foto` varchar(20) NOT NULL,
  PRIMARY KEY (`idusuario`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `usuario`
--

INSERT INTO `usuario` (`idusuario`, `nome`, `email`, `senha`, `foto`) VALUES
(1, 'Felipe da Silva Arraes', 'felipearraes28@gmail.com', '123', ''),
(2, 'iam', 'iam@gmail.com', '123', ''),
(3, 'Rainer', 'r@gmail.com', '123', ''),
(5, 'Joao', 'joao@gmail.com', '123', ''),
(6, 'Maria', 'maria@gmail.com', '123', '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuario_has_projeto`
--

DROP TABLE IF EXISTS `usuario_has_projeto`;
CREATE TABLE IF NOT EXISTS `usuario_has_projeto` (
  `usuario_idusuario` int(10) UNSIGNED NOT NULL,
  `projeto_idprojeto` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`usuario_idusuario`,`projeto_idprojeto`),
  KEY `usuario_has_projeto_FKIndex1` (`usuario_idusuario`),
  KEY `usuario_has_projeto_FKIndex2` (`projeto_idprojeto`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `usuario_has_projeto`
--

INSERT INTO `usuario_has_projeto` (`usuario_idusuario`, `projeto_idprojeto`) VALUES
(1, 22),
(1, 23),
(1, 25),
(1, 26),
(2, 22),
(3, 22),
(3, 23),
(3, 25);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
