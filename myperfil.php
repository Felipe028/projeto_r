<?php
$sql = BD::getconn()->prepare("SELECT * FROM usuario where idusuario = ?");
$sql->execute(array($_SESSION['idusuario']));
$ln = $sql->fetchObject();

?>
            <div class="col-xl-11 col-lg-7" style="margin: 0 auto;">
              <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                  <h6 class="m-0 font-weight-bold text-primary">Projetos</h6>
                  <div class="dropdown no-arrow">
                    <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <i class="ui-icon ui-icon-caret-1-s"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
                      <a class="dropdown-item" href="#" id="novoprojeto">Novo Projeto</a>
                      <a class="dropdown-item" href="#" id="pesquisar">Pesquisar</a>
                    </div>
                  </div>
                </div>
                <!-- Card Body -->
                <div class="card-body">
                  
                    
            <div class="row">        
                    
                    
                    
            <div class="col-xl-3 col-lg-7" style="margin: 0 auto;">
              <div class="card shadow mb-4">
                <!-- Card Body -->
                <div class="card-body" style="padding: 0px;">
<ul id="menu" style="list-style: none; border: none; -moz-border-radius: 3px; -webkit-border-radius:3px; padding: 5px 0px 5px 0px; width: 100%; margin: -5px 0px -5px 0px;">
  <li class="ui-widget-header" style="border: none; padding: 0px 0px 0px 5px;">Meus projetos</li>
  <?php
  $sqlmyproj = BD::getconn()->prepare("SELECT * FROM projeto where usuario_idusuario = ? ORDER BY nome ASC");
  $sqlmyproj->execute(array($_SESSION['idusuario']));
  while($lnmyproj = $sqlmyproj->fetchObject()){ ?>
  <li class="menu" style="border: none;"><div style="padding: 0px;"><a href="index.php?p=perf&p2=mypr&codpr=<?php echo $lnmyproj->idprojeto; ?>" style="text-decoration:none;"><div style="width: 100%; padding: 5px 0px 5px 10px;"><?php echo $lnmyproj->nome;?></div></a></div></li>
  <?php } ?>
  <li class="ui-widget-header" style="border: none; padding: 0px 0px 0px 5px;">Projetos envolvidos</li>
  <?php
  $sqlotherproj = BD::getconn()->prepare("SELECT * FROM usuario_has_projeto where usuario_idusuario = ?");
  $sqlotherproj->execute(array($_SESSION['idusuario']));
  while($lnotherproj = $sqlotherproj->fetchObject()){
      $sqlotherproj2 = BD::getconn()->prepare("SELECT * FROM projeto where idprojeto = ?");
	  $sqlotherproj2->execute(array($lnotherproj->projeto_idprojeto));
	  $lnotherproj2 = $sqlotherproj2->fetchObject();

	  if($_SESSION['idusuario'] != $lnotherproj2->usuario_idusuario){	  ?>
	  <li class="menu" style="border: none;"><div style="padding: 0px;"><a href="index.php?p=perf&p2=otpr&codpr=<?php echo $lnotherproj2->idprojeto;?>" style="text-decoration:none;"><div style="width: 100%; padding: 5px 0px 5px 10px; "><?php echo $lnotherproj2->nome;?></div></a></div></li>
  <?php }
  } ?>
</ul>                </div>
              </div>
            </div>
                
                
                
            <div class="col-xl-9 col-lg-7" style="padding: 0px;">
              <div class="card shadow mb-4" style="padding: 0px;">
                  <?php if($sqlmyproj->rowCount() > 0 || $sqlotherproj->rowCount() > 0 ){include "pagina2.php";}?>
              </div>
            </div>    
            
			
            </div>        
                    
                    
                    
                </div>
              </div>
            </div>

<!--NOVO PROJETO-->
<style>
    label, input { display:block; }
    input.text { margin-bottom:12px; width:95%; padding: .4em; }
    fieldset { padding:0; border:0; margin-top:25px; }
    h1 { font-size: 1.2em; margin: .6em 0; }
    div#users-contain { width: 100%; margin: 20px 0; }
    div#users-contain table { margin: 1em 0; border-collapse: collapse; width:100%;}
    div#users-contain table td, div#users-contain table th { border: 1px solid #eee; padding: .6em 10px; text-align: left; width:50%;}
    .ui-dialog .ui-state-error { padding: .3em; }
    .validateTips { border: 1px solid transparent; padding: 0.3em; }
</style>
<script>
   $( function() {
    var dialog;
 
    dialog = $( "#dialog-form" ).dialog({
      autoOpen: false,
      height: 500,
      width: 400,
      modal: true,
      buttons: {
        Cadastrar: function() {
          $("#meuformdialog").submit();
        },
        Cancelar: function() {
          dialog.dialog( "close" );
        }
      },
    });

	$( "#novoprojeto" ).button().on( "click", function() {
      dialog.dialog( "open" );
    });	
  });
</script>
<!--##POPUP##-->
<!---->
<div id="dialog-form" title="Novo projeto">
  <form method="post" action="funcoes/projetos/inserir.php" id="meuformdialog" enctype="multipart/form-data">
      <label for="name">Nome do projeto</label>
      <input type="text" name="nome" id="nome" class="text ui-widget-content ui-corner-all">
	  <label for="name">Início</label>
      <input type="date" name="inicio" id="inicio" class="text ui-widget-content ui-corner-all">
	  <label for="name">Término</label>
      <input type="date" name="termino" id="termino" class="text ui-widget-content ui-corner-all">
	  <label for="name">Descrição</label>
      <textarea name="descricao" id="descricao" class="text ui-widget-content ui-corner-all" rows="6" style="width: 98%;"></textarea>
	  <label for="name">Cronograma</label>
	  <input type="file" name="arquivo" id="arquivo" style="background: #f4f7ee; height: 22px;"/>
  </form>
</div>
<!--##FIM POPUP##-->	
<!--fim Novo projeto-->	





<!--Busca-->
<!--<script type="text/javascript" src="js/jquery-1.12.0.min.js"></script>-->
<script type="text/javascript">
		    $(function(){
				$("#pesquisa2").keyup(function(){
					var pesquisa = $(this).val();
					if(pesquisa != ''){
						var dados = { palavra : pesquisa, palavra2 : "2"}

						$.post('busca_projeto.php', dados, function(retorna){
							$(".resultados2").html(retorna);
						});
					}
				});
			});						
</script>
<!--Fim Busca-->
<!--PESQUISAR-->
<script>
   $( function() {
    var dialog;
 
    dialog = $( "#dialog-form_pesquisar" ).dialog({
      autoOpen: false,
      height: 500,
      width: 400,
      modal: true,
    });

	$( "#pesquisar" ).button().on( "click", function() {
      dialog.dialog( "open" );
    });	
  });
</script>
<!--##POPUP##-->
<!---->
<div id="dialog-form_pesquisar" title="Buscar projeto">
  <form action="" method="post">
      <input type="text" name="pesquisa2" id="pesquisa2" placeholder="Digite o nome do projeto..." class="text ui-widget-content ui-corner-all">
  </form>
  <ul class="resultados2" style="list-style: none; border: none; margin: 0px 0px 0px -40px; width: 98%;"></ul>
</div>
<!--##FIM POPUP##-->	
<!--fim Pesquisar-->	