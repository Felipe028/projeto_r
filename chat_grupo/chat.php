<?php
include_once "../verifica_login.php";
include_once "../chat/config/define.php";
require_once('../chat/classes/BD.class.php');
$sql = BD::getconn()->prepare("SELECT * FROM mensagens_grupo where projeto_idprojeto = ? ORDER BY idmensagens_grupo ASC");
$sql->execute(array($_POST['id_projeto']));
while($ln = $sql->fetchObject()){
	$sql2 = BD::getconn()->prepare("SELECT * FROM usuario where idusuario = ?");
	$sql2->execute(array($ln->usuario_idusuario));
    $ln2 = $sql2->fetchObject();
?>
<p style="width: 450px; float: left;">
<div class="<?php if($_SESSION['idusuario'] == $ln->usuario_idusuario){ echo "div_chat_grupo_logado";}else{echo "div_chat_grupo_outros";}?>">
	    <?php if($_SESSION['idusuario'] != $ln->usuario_idusuario){?>
		<p class="usuario" style="color: #6576df;"><?php echo $ln2->nome;?></p>
		<?php } ?>
	    <p class="mensagem"><?php echo nl2br($ln->mensagem); ?></p>
	    <p class="data-hora"><?php echo date('H:m - d/m/Y', strtotime($ln->datahora)); ?></p>
</div>
</p>
<?php } ?>