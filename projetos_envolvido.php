<?php
if(!isset($_GET['codpr'])){//se cod não existir
    $sql = BD::getconn()->prepare("SELECT * FROM projeto where usuario_idusuario = ? ORDER BY idprojeto DESC LIMIT 1");
	$sql->execute(array($_SESSION['idusuario']));
}else{
	$sql = BD::getconn()->prepare("SELECT * FROM projeto where idprojeto = ?");
	$sql->execute(array($_GET['codpr']));
}
$ln = $sql->fetchObject();

$sql_autor = BD::getconn()->prepare("SELECT * FROM usuario where idusuario = ?");
$sql_autor->execute(array($ln->usuario_idusuario));
$ln_autor = $sql_autor->fetchObject();
?>

<div class="divtopo3">
<p><?php echo $ln->nome; ?></p>
</div>

<div class="divcorpo3_detalhes">
<label for="name">Nome</label>
<p><?php echo $ln->nome; ?></p>
<label for="name">Autor</label>
<p><?php echo $ln_autor->nome; ?></p>
<label for="name">Início</label>
<p><?php echo date('d-m-Y', strtotime($ln->inicio)); ?></p>
<label for="name">Término</label>
<p><?php echo date('d-m-Y', strtotime($ln->termino)); ?></p>
<label for="name">Descrição</label>
<p><?php echo $ln->descricao; ?></p>
<?php

$sql2 = BD::getconn()->prepare("SELECT * FROM usuario_has_projeto where usuario_idusuario = ? AND projeto_idprojeto = ?");
$sql2->execute(array($_SESSION['idusuario'], $_GET['codpr']));
if($sql2->rowCount() > 0 && $ln->arquivo != NULL){ ?>
<label for="name">Cronograma</label>
<p><a href="arquivos/<?php echo $ln->arquivo; ?>" download>Clique aqui para fazer o download do arquivo</a></p>
<?php } ?>
</div>

<div class="divtopo3" style="margin: 10px 0px 0px 0px;">
<p>Membros</p>
</div>

<div class="divcorpo3_membros">
<?php
$sql_membros = BD::getconn()->prepare("SELECT * FROM usuario_has_projeto where projeto_idprojeto = ?");
$sql_membros->execute(array($ln->idprojeto));
//$sqlpro = mysql_query("SELECT * FROM projeto where idprojeto = '".$ln['idprojeto']."'");
//$lnpro = mysql_fetch_array($sqlpro);
?>
<ul style="list-style: none; border: none; margin: 0px 0px 5px -40px;" class="listaprojeto">
<?php
while ($ln_membros = $sql_membros->fetchObject()){
$sql_user = BD::getconn()->prepare("SELECT * FROM usuario where idusuario = ?");
$sql_user->execute(array($ln_membros->usuario_idusuario));

$ln_user = $sql_user->fetchObject();
?>

<li><a href="index.php?p=perf&cod=<?php echo $ln_membros->usuario_idusuario;?>" style="text-decoration:none;"><div class="div" style="padding: 2px 0px 5px 10px; height: 30px;">
<div style="margin: 8px 0px 0px 0px;"><?php echo $ln_user->nome;?></div>
  </div>
  </a>
</li>
 
 
<?php } ?>
</ul>

</div>

<link href="chat_grupo/css/style.css" rel="stylesheet" type="text/css">   
<div class="divtopo3" >
<p>Conversas</p>
</div>
<?php
include "chat_grupo/chat-index.php";
?>
