<div class="jumbotron">
  <h1 class="display-4">Olá, bem vindo ao nosso chat!</h1>
  <p class="lead">Use nosso chat para conversar em tempo real com seus amigos e discutir sobre os seus projetos.</p>
  <hr class="my-4">
  <p>Caso seja necessário, temos também o chat em grupo para você discutir com um grupo de pessoas sobre os projetos em que você está envolvido.</p>
  
</div>