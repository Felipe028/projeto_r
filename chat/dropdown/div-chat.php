<?php
include_once "../../verifica_login.php";
include_once "../config/define.php";
require_once('../classes/BD.class.php');


$sql = BD::getconn()->prepare("SELECT * FROM mensagens WHERE `id_para` = ? AND `lido` = 0 ORDER BY `id` DESC");
$sql->execute(array($_POST['id_user']));
while($ln = $sql->fetchObject()){
	$sql2 = BD::getconn()->prepare("SELECT * FROM usuario WHERE `idusuario` = ?");
	$sql2->execute(array($ln->id_de));
	$ln2 = $sql2->fetchObject();
?>
                <a class="dropdown-item d-flex align-items-center" href="index.php?p=msg&cod=<?php echo $ln2->idusuario?>">
                  <div class="dropdown-list-image mr-3">
				    <?php if($ln2->foto != NULL || $ln2->foto != ''){ ?>
						<img src="../foto_perfil/<?php echo $ln2->foto;?>" class="rounded-circle" style="width: 45px; heigth: 45px;"/>
						<?php
					  }else{ ?>
						<img src="../foto_perfil/default.jpg" class="rounded-circle" style="width: 45px; heigth: 45px;"/>
						<?php
					  } ?>
                    <div class="status-indicator bg-success"></div>
                  </div>
                  <div class="font-weight-bold">
                    <div class="text-truncate"><?php echo nl2br($ln->mensagem);?></div>
                    <div class="small text-gray-500"><?php echo $ln2->nome ."  -  ". date("H:i - d/m/Y", strtotime($ln->datahora));?></div>
                  </div>
                </a>
<?php } ?>