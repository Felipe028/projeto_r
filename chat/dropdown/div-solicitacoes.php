<?php
include_once "../../verifica_login.php";
include_once "../config/define.php";
require_once('../classes/BD.class.php');



$sql2 = BD::getconn()->prepare("SELECT * FROM solicitacoes where id_para = ?");
$sql2->execute(array($_SESSION['idusuario']));
while($ln2 = $sql2->fetchObject()){
	$sql3 = BD::getconn()->prepare("SELECT * FROM usuario where idusuario = ?");
    $sql3->execute(array($ln2->id_de));
    $ln3 = $sql3->fetchObject();
?>
<script type="text/javascript">
$(function(){
   $("#botao-aceitar<?php echo $ln3->idusuario;?>").click(function(){
       
	   var id1 = "<?php echo $_SESSION['idusuario']; ?>";
	   var id2 = "<?php echo $ln3->idusuario; ?>";
       $.post('../solicitacoes/aceitar.php', {
           id_de: id2, id_para: id1
       }, function(retorna){
           $(".resultados3").html(retorna);
       });
   });

   $("#botao-recusar<?php echo $ln3->idusuario;?>").click(function(){
       
	   var id1 = "<?php echo $_SESSION['idusuario']; ?>";
	   var id2 = "<?php echo $ln3->idusuario; ?>";
       $.post('../solicitacoes/recusar.php', {
           id_de: id2, id_para: id1
       }, function(retorna){
           $(".resultados3").html(retorna);
       });
   });
})
</script>
                <a class="dropdown-item d-flex align-items-center" href="#">
                  <div class="dropdown-list-image mr-3">
				    <?php if($ln3->foto != NULL || $ln3->foto != ''){ ?>
						<img src="../foto_perfil/<?php echo $ln3->foto;?>" class="rounded-circle"/>
						<?php
					  }else{ ?>
						<img src="../foto_perfil/default.jpg" class="rounded-circle"/>
						<?php
					  } ?>
                    <div class="status-indicator bg-success"></div>
                  </div>
                  <div class="font-weight-bold">
                    <div class="text-truncate"><?php echo $ln3->nome; ?></div>
                    <div style="text-align: center;">
                        <button id="botao-aceitar<?php echo $ln3->idusuario;?>" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">Aceitar</button>
                        <button id="botao-recusar<?php echo $ln3->idusuario;?>" class="d-none d-sm-inline-block btn btn-sm btn-secondary shadow-sm">Recusar</button>
                    </div>
                  </div> 
                </a>
<?php } ?>