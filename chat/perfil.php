<?php
$sqlUser = BD::getconn()->prepare("SELECT * FROM usuario WHERE idusuario = ?");
$sqlUser->execute(array($_GET['cod']));
$lnUser = $sqlUser->fetchObject();
?>
 
 <div class="row">

            <!-- Area Chart -->
            <div class="col-xl-8 col-lg-7" style="margin: 0 auto;">
              <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                  <h6 class="m-0 font-weight-bold text-primary"><?php echo $lnUser->nome;?></h6>
                  <?php
					$sqlCont = BD::getconn()->prepare("SELECT * FROM amigos WHERE id_amigo = ? AND usuario_idusuario = ?");
					$sqlCont->execute(array($lnUser->idusuario, $_SESSION['idusuario']));
					if($sqlCont->rowCount() != 0){
					?>
				  <div class="dropdown no-arrow">
                    <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
                      <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">Excluir da lista de amigos</a>
                    </div>
                  </div>
				  <?php
				    }else{
						$sqlCont = BD::getconn()->prepare("SELECT * FROM solicitacoes WHERE id_de = ? AND id_para = ?");
						$sqlCont->execute(array($_SESSION['idusuario'], $lnUser->idusuario));
						if($sqlCont->rowCount() != 0){ ?>
							<button type="button" class="btn btn-primary btn-sm disabled" data-toggle="modal">Enviar Solicitação</button>
						<?php
						}else{ ?>
						    <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#exampleModal" >Enviar Solicitação</button>
						<?php } } ?>
				  <!-- Modal -->
				  <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
				    <div class="modal-dialog" role="document">
					  <div class="modal-content">
					    <div class="modal-header">
						  <h5 class="modal-title" id="exampleModalLabel">Enviar solicitação de amizade</h5>
						  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
						    <span aria-hidden="true">&times;</span>
					      </button>
						</div>
					    <div class="modal-body">
						  Deseja enviar solicitação de amizade para <b><?php echo $lnUser->nome;?></b>
						</div>
						<div class="modal-footer">
						  <button type="button" class="btn btn-secondary" data-dismiss="modal">Não</button>
						    <form action="solicitacoes/enviar.php" method="post">
							  <input type="text" name="cod" value="<?php echo $_GET['cod'];?>" style="display:none;">
							  <button type="submit" class="btn btn-primary">Sim</button>
							</form>
						</div>
					  </div>
					</div>
				  </div>
                </div>
				
				<div>
				  <div style="float: left; margin: 10px;">
				    <?php if($lnUser->foto != NULL || $lnUser->foto != ''){ ?>
						<img src="../foto_perfil/<?php echo $lnUser->foto;?>" class="img-profile" style="width: 150px; heigth: 150px;"/>
						<?php
					  }else{ ?>
						<img src="../foto_perfil/default.jpg" class="img-profile"/>
						<?php
					  } ?>
				    
				  </div>
				  <div style="float: left; margin: 20px 0px 10px 0px;">
				    <h2 class="display-7"><?php echo $lnUser->nome;?></h2>
					<p class="lead"><?php echo $lnUser->email;?></p>
				  </div>
				</div>
				
				
				
				
				
<div id="accordion">
  <p class="lead" style="width: 100%; background: #b5b5b5; padding: 10px; color: #ffffff; margin: 0px;">Projetos de autoria própria</p>				
<?php
$sqlproj = BD::getconn()->prepare("SELECT * FROM projeto WHERE usuario_idusuario = ? ORDER BY `nome` ASC");
$sqlproj->execute(array($_GET['cod']));
while($lnproj = $sqlproj->fetchObject()){ ?>

  <div class="card">
    <div class="card-header" id="headingOne">
      <h5 class="mb-0">
        <button class="btn btn-link" data-toggle="collapse" data-target="#collapse<?php echo $lnproj->idprojeto;?>" aria-expanded="true" aria-controls="collapse<?php echo $lnproj->idprojeto;?>">
          <?php echo $lnproj->nome; ?>
        </button>
      </h5>
    </div>

    <div id="collapse<?php echo $lnproj->idprojeto;?>" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
      <div class="card-body">
        Descrição: <?php echo nl2br($lnproj->descricao);?>
      </div>
    </div>
  </div>

<?php
} ?>				
</div>				
				
				
				
				
				
				
				
				
<div id="accordion">
  <p class="lead" style="width: 100%; background: #b5b5b5; padding: 10px; color: #ffffff; margin: 20px 0px 0px 0px;">Projetos envolvidos</p>
<?php
$sqlproj2 = BD::getconn()->prepare("SELECT * FROM usuario_has_projeto WHERE usuario_idusuario = ?");
$sqlproj2->execute(array($_GET['cod']));
while($lnproj2 = $sqlproj2->fetchObject()){
	$sqlproj3 = BD::getconn()->prepare("SELECT * FROM projeto WHERE idprojeto = ?");
    $sqlproj3->execute(array($lnproj2->projeto_idprojeto));
	$lnproj3 = $sqlproj3->fetchObject();
	?>

  <div class="card">
    <div class="card-header" id="headingOne">
      <h5 class="mb-0">
        <button class="btn btn-link" data-toggle="collapse" data-target="#collapse2<?php echo $lnproj3->idprojeto;?>" aria-expanded="true" aria-controls="collapse<?php echo $lnproj3->idprojeto;?>">
          <?php echo $lnproj3->nome; ?>
        </button>
      </h5>
    </div>

    <div id="collapse2<?php echo $lnproj3->idprojeto;?>" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
      <div class="card-body">
	    Descrição: <?php echo nl2br($lnproj3->descricao);?>
      </div>
    </div>
  </div>

<?php
} ?>  
</div>				
				
				
				
				
				
				
				
				
				
              </div>
            </div> 
          </div>
		  
		  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Excluir da lista de amigos</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Dejesa realmente excluir <b><?php echo $lnUser->nome; ?></b> da sua lista de amigos?</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
		  <form method="post" action="../funcoes/amigos/excluir.php">
		    <input type="hidden" name="cod" value="<?php echo $lnUser->idusuario; ?>" />
			<input type="submit" value="Excluir" class="btn btn-primary" />
		  </form>
        </div>
      </div>
    </div>
  </div>