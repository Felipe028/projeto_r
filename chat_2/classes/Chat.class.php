<?php
class Chat{
	private $nome;
	private $id_de;
	private $id_para;
	private $mensagem;
	private $tempoLimite;

	public function __construct(){
//		$this->tempoLimite = TEMPO_LIMITE;
	}

	public function setNome($nome){
		$this->nome = $nome;
	}
	public function setId_de($id_de){
		$this->id_de = $id_de;
	}
	public function setId_para($id_para){
		$this->id_para = $id_para;
	}
	public function getNome(){
		return $this->nome;
	}
	public function getId_de(){
		return $this->id_de;
	}
	public function getId_para(){
		return $this->id_para;
	}
	public function setMensagem($msg){
		$this->mensagem = $msg;
	}
	public function getMensagem(){
		return $this->mensagem;
	}

	public function inserir(){
		$stmt  = BD::getConn()->prepare("INSERT INTO mensagens SET id_de = ?, id_para = ?, mensagem = ? , lido = 0, datahora = NOW()");
		$data  = array($this->getId_de(), $this->getId_para(), $this->getMensagem());
		return $stmt->execute($data);
	}

	public function existeNome(){
		$stmt  = BD::getConn()->prepare("SELECT COUNT(nome) FROM chat WHERE nome = ?");
		$data  = array($this->getNome());
		$stmt->execute($data);
		return ($stmt->fetchColumn() > 0) ? true : false;
	}
	public function excluir(){
//		$stmt   = BD::getConn()->query("DELETE FROM chat WHERE DATE_ADD(datahora, INTERVAL {$this->tempoLimite} DAY) < NOW()");
	}
	public function listar($a, $b){
		return BD::getConn()->query("SELECT * FROM mensagens WHERE (`id_de` = $a AND `id_para` = $b) OR (`id_de` = $b AND `id_para` = $a) ORDER BY `id` DESC");
	}
}
?>